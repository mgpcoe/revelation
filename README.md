Project Revelation
---

An attempt, in the early 2000s, to implement RSA's MD5 hashing algorithm in JavaScript. SSL was hard and expensive then,
and I want a way to allow people to use industry-standard cryptography when sending passwords over unencrypted channels.
It supports HMAC, too, because I'm not a complete backbirth (though, obviously, the salt is still sent in the clear,
but that seemed a like necessary evil at the time), but I never fully convinced myself I'd correctly implemented MD5.

Nonetheless, here, because I'm still proud of the attempt, and I wanted to get it off SourceForge.
