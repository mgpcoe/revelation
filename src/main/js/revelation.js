/*
   Project Revelation Copyright (C) 2002, Prophecy Newmedia
   All rights reserved.

   JavaScript v1.3 Implementation of RSA Data Security, Inc. MD5 Message-Digest Algorithm

   Redistribution and use in source and binary forms, with or without modification, are
   permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.
    * Neither the name of the Prophecy Newmedia nor the names of its contributors may be
	  used to endorse or promote products derived from this software without specific prior
	  written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
   THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

function negateString( string ) {
	document.write( "<BR>Line 31&nbsp;&nbsp;Beginning function negateString( " + string );
	document.write( " )...<BR>" );
	var negatedString;
	var currentChar;
	
	negatedString = "";
	for( var i = 0; i < string.length; i++ ) {
		currentChar = string.charAt( i );
		document.write( "Line 39&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;currentChar = " + currentChar )
		document.write( "<BR>" );
		document.write( "Line 41&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;currentChar.charCodeAt( 0 ) = " + currentChar.charCodeAt( 0 ) + "<BR>" );
		document.write( "Line 42&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;65535 - currentChar.charCodeAt( 0 ) = " + ( 65535 - currentChar.charCodeAt( 0 ) ) + "<BR>" );
		negatedString += String.fromCharCode( 65535 - currentChar.charCodeAt( 0 ) );
		document.write( "Line 44&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;negatedString = " + negatedString );
		document.write( "<BR>" );
	}

	document.write( "Line 48&nbsp;&nbsp;Function negateString complete, returning value " + negatedString );
	document.write( "...<BR><BR>" );
	return negatedString;
}

function getOR( string1, string2 ) {
	document.write( "<BR>Line 54&nbsp;&nbsp;Beginning function getOR( " + string1 );
	document.write( ", " + string2 );
	document.write( " )...<BR>" );
	var orResults;
	var current1;
	var current2;

	orResults = "";
	document.write( "Line 62&nbsp;&nbsp;string1.length = " + string1.length + "<BR>" );
	for( var i = 0; i < string1.length; i++ ) {
		current1 = string1.charAt( i );
		current2 = string2.charAt( i );
		document.write( "Line 66&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1 = " + current1 );
		document.write( "<BR>" );
		document.write( "Line 68&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1.charCodeAt( 0 ) = " + current1.charCodeAt( 0 ) + "<BR>" );
		document.write( "Line 69&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2 = " + current2 );
		document.write( "<BR>" );
		document.write( "Line 71&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2.charCodeAt( 0 ) = " + current2.charCodeAt( 0 ) + "<BR>" );
		orResults += String.fromCharCode( current1.charCodeAt( 0 ) | current2.charCodeAt( 0 ) );
		document.write( "Line 73&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;orResults = " + orResults );
		document.write( "<BR>" );
	}

	document.write( "Line 77&nbsp;&nbsp;Function getOR complete, returning value " + orResults );
	document.write( "...<BR><BR>" );
	return orResults;
}

function getAND( string1, string2 ) {
	document.write( "<BR>Line 83&nbsp;&nbsp;Beginning function getAND( " + string1 );
	document.write( ", " + string2 );
	document.write( " )...<BR>" );
	var andResults;
	var current1;
	var current2;

	andResults = "";
	document.write( "Line 91&nbsp;&nbsp;string1.length = " + string1.length + "<BR>" );
	for( var i = 0; i < string1.length; i++ ) {
		current1 = string1.charAt( i );
		current2 = string2.charAt( i );
		document.write( "Line 95&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1 = " + current1 );
		document.write( "<BR>" );
		document.write( "Line 97&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1.charCodeAt( 0 ) = " + current1.charCodeAt( 0 ) + "<BR>");
		document.write( "Line 98&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2 = " + current2 );
		document.write( "<BR>" );
		document.write( "Line 100&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2.charCodeAt( 0 ) = " + current2.charCodeAt( 0 ) + "<BR>" );
		andResults += String.fromCharCode( current1.charCodeAt( 0 ) & current2.charCodeAt( 0 ) );
		document.write( "Line 102&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;andResults = " + andResults );
		document.write( "<BR>" );
	}

	document.write( "Line 106&nbsp;&nbsp;Function getAND complete, returning value " + andResults );
	document.write( "...<BR><BR>" );
	return andResults;
}

function getXOR( string1, string2 ) {
	document.write( "<BR>Line 112&nbsp;&nbsp;Beginning function getXOR( " + string1 );
	document.write( ", " + string2 );
	document.write( " )...<BR>" );
	var xorResults;
	var current1;
	var current2;

	xorResults = "";
	document.write( "Line 120&nbsp;&nbsp;string1.length = " + string1.length + "<BR>" );
	for( var i = 0; i < string1.length; i++ ) {
		current1 = string1.charAt( i );
		current2 = string2.charAt( i );
		document.write( "Line 124&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1 = " + current1 );
		document.write( "<BR>" );
		document.write( "Line 126&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current1.charCodeAt( 0 ) = " + current1.charCodeAt( 0 ) + "<BR>" );
		document.write( "Line 127&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2 = " + current2 );
		document.write( "<BR>" );
		document.write( "Line 129&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;current2.charCodeAt( 0 ) = " + current2.charCodeAt( 0 ) + "<BR>" );
		xorResults += String.fromCharCode( current1.charCodeAt( 0 ) ^ current2.charCodeAt( 0 ) );
		document.write( "Line 131&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;xorResults = " + xorResults );
		document.write( "<BR>" );
	}

	document.write( "Line 135&nbsp;&nbsp;Function getXOR complete, returning value " + xorResults );
	document.write( "...<BR><BR>" );
	return xorResults;
}

function addWords( word1, word2 ) {
	document.write( "<BR>Line 141&nbsp;&nbsp;Beginning function addWords( " + word1 );
	document.write( ", " + word2 );
	document.write( " )...<BR>" );
	word1Codes = new Array( 2 );
	word2Codes = new Array( 2 );
	theWordCodes = new Array( 2 );
	var secondByteOverflow;
	var theString;
	var i;
	var j;

	for( i = 1; i <= 2; i++ ) {
		for( j = 0; j <= 1; j++ ) {
			eval( "word" + i + "Codes[ " + j + " ] = word" + i + ".charCodeAt( " + j + " )" );
			document.write( "Line 155&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;" );
			document.write( "word" + i + "Codes[ " + j + " ] = word" + i + ".charCodeAt( " + j + " ) = " );
			document.write( eval( "word" + i + "Codes[ " + j + " ] = word" + i + ".charCodeAt( " + j + " )" ) );
			document.write( "<BR>" );
		}
	}

	for( i = 0; i <= 1; i++ ) {
		theWordCodes[ i ] = word1Codes[ i ] + word2Codes[ i ];
		document.write( "Line 164&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;theWordCodes[ i ] = " + theWordCodes[ i ] + "<BR>" );
	}

	document.write( "Line 167&nbsp;&nbsp;theWordCodes[ 1 ] = " + theWordCodes[ 1 ] + "<BR>" );
	if( theWordCodes[ 1 ] > 65535 ) {
		secondByteOverflow = theWordCodes[ 1 ] - 65536;
		document.write( "Line 170&nbsp;&nbsp;secondByteOverflow = " + secondByteOverflow + "<BR>" );
		theWordCodes[ 1 ] -= 65536;
		document.write( "Line 172&nbsp;&nbsp;theWordCodes[ 1 ] = " + theWordCodes[ 1 ] + "<BR>" );
		secondByteOverflow = 65535 - secondByteOverflow;
		document.write( "Line 174&nbsp;&nbsp;secondByteOverflow = " + secondByteOverflow + "<BR>" );
		theWordCodes[ 0 ] += secondByteOverflow;
		document.write( "Line 176&nbsp;&nbsp;theWordCodes[ 0 ] = " + theWordCodes[ 0 ] + "<BR>" );
	}

	document.write( "Line 179&nbsp;&nbsp;theWordCodes[ 0 ] = " + theWordCodes[ 0 ] + "<BR>" );
	if( theWordCodes[ 0 ] > 65535 ) {
		theWordCodes[ 0 ] = theWordCodes[ 0 ] - ( theWordCodes[ 0 ] & 4294901760 );
		document.write( "Line 182&nbsp;&nbsp;theWordCodes[ 0 ] = " + theWordCodes[ 0 ] + "<BR>" );
	}

	theString = String.fromCharCode( theWordCodes[ 0 ] );
	document.write( "Line 186&nbsp;&nbsp;theString = " + theString );
	document.write( "<BR>" );
	theString += String.fromCharCode( theWordCodes[ 1 ] );
	document.write( "Line 189&nbsp;&nbsp;theString = " + theString );
	document.write( "<BR>" );
	
	document.write( "Line 192&nbsp;&nbsp;Function addWords complete, returning value " + theString );
	document.write( "...<BR><BR>" );
	return theString;
}

function MDfuncF( x, y, z ) {
	document.write( "<BR>Line 198&nbsp;&nbsp;Beginning function MDfuncF( " + x );
	document.write( ", " + y );
	document.write( ", " + z );
	document.write( " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var negX;

	negX = negateString( x );
	document.write( "Line 208&nbsp;&nbsp;negX = " + negX );
	document.write( "<BR>" );
	stepOne = getAND( x, y );
	document.write( "Line 211&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = getOR( stepOne, negX );
	document.write( "Line 214&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = getAND( stepTwo, z );
	document.write( "Line 217&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );

	document.write( "Line 220&nbsp;&nbsp;Function MDfuncF complete, returning value " + stepThree );
	document.write( "...<BR><BR>" );
	return stepThree;
}

function MDfuncG( x, y, z ) {
	document.write( "<BR>Line 226&nbsp;&nbsp;Beginning function MDfuncG( " + x );
	document.write( ", " + y );
	document.write( ", " + z );
	document.write( " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var negZ;

	negZ = negateString( z );
	document.write( "Line 236&nbsp;&nbsp;negZ = " + negZ );
	document.write( "<BR>" );
	stepOne = getAND( x, z );
	document.write( "Line 239&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = getOR( stepOne, y );
	document.write( "Line 242&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = getAND( stepTwo, negZ );
	document.write( "Line 245&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );

	document.write( "Line 248&nbsp;&nbsp;Function MDfuncG complete, returning value " + stepThree );
	document.write( "...<BR><BR>" );
	return stepThree;
}

function MDfuncH( x, y, z ) {
	document.write( "<BR>Line 254&nbsp;&nbsp;Beginning function MDfuncH( " + x );
	document.write( ", " + y );
	document.write( ", " + z );
	document.write( " )...<BR>" );
	var stepOne;
	var stepTwo;

	stepOne = getXOR( x, y );
	document.write( "Line 262&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = getXOR( stepOne, z );
	document.write( "Line 265&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );

	document.write( "Line 268&nbsp;&nbsp;Function MDfuncH complete, returning value " + stepTwo );
	document.write( "...<BR><BR>" );
	return stepTwo;
}

function MDfuncI( x, y, z ) {
	document.write( "<BR>Line 274&nbsp;&nbsp;Beginning function MDfuncI( " + x );
	document.write( ", " + y );
	document.write( ", " + z );
	document.write( " )...<BR>" );
	var stepOne;
	var stepTwo;
	var negZ;

	negZ = negateString( z );
	document.write( "Line 283&nbsp;&nbsp;negZ = " + negZ );
	document.write( "<BR>" );
	stepOne = getOR( x, negZ );
	document.write( "Line 286&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = getXOR( y, stepOne );
	document.write( "Line 289&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );

	document.write( "Line 292&nbsp;&nbsp;Function MDfuncI complete, returning value " + stepTwo );
	document.write( "...<BR><BR>" );
	return stepTwo;
}

function leftShiftWord( word, shift ) {
	document.write( "<BR>Line 298&nbsp;&nbsp;Beginning function leftShiftWord( " + word );
	document.write( ", " + shift + " )...<BR>" );
	wordCodes = new Array( 2 );
	var char1;
	var char2;
	var shiftedOut;
	var newChar1Code;
	var newChar2Code;
	var theWord;

	char1 = word.charAt( 0 );
	char2 = word.charAt( 1 );
	document.write( "Line 310&nbsp;&nbsp;char1 = " + char1 );
	document.write( "<BR>" );
	document.write( "Line 312&nbsp;&nbsp;char1.charCodeAt( 0 ) = " + char1.charCodeAt( 0 ) + "<BR>" );
	document.write( "Line 313&nbsp;&nbsp;char2 = " + char2 );
	document.write( "<BR>" );
	document.write( "Line 315&nbsp;&nbsp;char2.charCodeAt( 0 ) = " + char2.charCodeAt( 0 ) + "<BR>" );
	wordCodes[ 0 ] = char1.charCodeAt( 0 );
	document.write( "Line 317&nbsp;&nbsp;wordCodes[ 0 ] = " + wordCodes[ 0 ] + "<BR>" );
	wordCodes[ 1 ] = char2.charCodeAt( 0 );
	document.write( "Line 319&nbsp;&nbsp;wordCodes[ 1 ] = " + wordCodes[ 1 ] + "<BR>" );

	if( shift > 16 ) {
		shiftedOut = wordCodes[ 0 ];
		document.write( "Line 323&nbsp;&nbsp;shiftedOut = " + shiftedOut + "<BR>" );
		wordCodes[ 0 ] = wordCodes[ 1 ];
		document.write( "Line 325&nbsp;&nbsp;wordCodes[ 0 ] = " + wordCodes[ 0 ] + "<BR>" );
		wordCodes[ 1 ] = shiftedOut;
		document.write( "Line 327&nbsp;&nbsp;wordCodes[ 1 ] = " + wordCodes[ 1 ] + "<BR>" );
		shift -= 16;
		document.write( "Line 329&nbsp;&nbsp;shift = " + shift + "<BR>" );
	}

	shiftedOut = ( wordCodes[ 0 ] >> ( 16 - shift ) );
	document.write( "Line 333&nbsp;&nbsp;shiftedOut = " + shiftedOut + "<BR>" );
	wordCodes[ 0 ] = ( ( ( Math.pow( 2, ( 16 - shift ) ) - 1 ) & wordCodes[ 0 ] ) << shift );
	document.write( "Line 335&nbsp;&nbsp;wordCodes[ 0 ] = " + wordCodes[ 0 ] + "<BR>" );
	wordCodes[ 0 ] += ( wordCodes[ 1 ] >> ( 16 - shift ) );
	document.write( "Line 337&nbsp;&nbsp;wordCodes[ 0 ] = " + wordCodes[ 0 ] + "<BR>" );
	wordCodes[ 1 ] = ( ( ( Math.pow( 2, ( 16 - shift ) ) - 1 ) & wordCodes[ 1 ] ) << shift );
	document.write( "Line 339&nbsp;&nbsp;wordCodes[ 1 ] = " + wordCodes[ 1 ] + "<BR>" );
	wordCodes[ 1 ] += shiftedOut;
	document.write( "Line 341&nbsp;&nbsp;wordCodes[ 1 ] = " + wordCodes[ 1 ] + "<BR>" );

	theWord = String.fromCharCode( wordCodes[ 0 ] );
	document.write( "Line 344&nbsp;&nbsp;theWord = " + theWord );
	document.write( "<BR>" );
	theWord += String.fromCharCode( wordCodes[ 1 ] );
	document.write( "Line 347&nbsp;&nbsp;theWord = " + theWord );
	document.write( "<BR>" );

	document.write( "Line 350&nbsp;&nbsp;Function leftShiftWord complete, returning value " + theWord );
	document.write( "...<BR><BR>" );
	return theWord;
}

function roundOne( a, b, c, d, k, s, i ) {
	document.write( "<BR>Line 356&nbsp;&nbsp;Beginning function roundOne( " + a );
	document.write( ", " + b );
	document.write( ", " + c );
	document.write( ", " + d );
	document.write( ", " + k );
	document.write( ", " + s + ", " + i + " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var stepFour;
	var stepFive;
	var stepSix;

	stepOne = MDfuncF( b, c, d );
	document.write( "Line 370&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = addWords( a, stepOne );
	document.write( "Line 373&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = addWords( stepTwo, k );
	document.write( "Line 376&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );
	stepFour = addWords( stepThree, numericToString( i ) );
	document.write( "Line 379&nbsp;&nbsp;stepFour = " + stepFour );
	document.write( "<BR>" );
	stepFive = leftShiftWord( stepFour, s );
	document.write( "Line 382&nbsp;&nbsp;stepFive = " + stepFive );
	document.write( "<BR>" );
	stepSix = addWords( b, stepFive );
	document.write( "Line 385&nbsp;&nbsp;stepSix = " + stepSix );
	document.write( "<BR>" );

	document.write( "Line 388&nbsp;&nbsp;Function roundOne complete, returning value " + stepSix );
	document.write( "...<BR><BR>" );
	return stepSix;
}

function roundTwo( a, b, c, d, k, s, i ) {
	document.write( "<BR>Line 394&nbsp;&nbsp;Beginning function roundTwo( " + a );
	document.write( ", " + b );
	document.write( ", " + c );
	document.write( ", " + d );
	document.write( ", " + k );
	document.write( ", " + s + ", " + i + " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var stepFour;
	var stepFive;
	var stepSix;

	stepOne = MDfuncG( b, c, d );
	document.write( "Line 408&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = addWords( a, stepOne );
	document.write( "Line 411&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = addWords( stepTwo, k );
	document.write( "Line 414&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );
	stepFour = addWords( stepThree, numericToString( i ) );
	document.write( "Line 417&nbsp;&nbsp;stepFour = " + stepFour );
	document.write( "<BR>" );
	stepFive = leftShiftWord( stepFour, s );
	document.write( "Line 420&nbsp;&nbsp;stepFive = " + stepFive );
	document.write( "<BR>" );
	stepSix = addWords( b, stepFive );
	document.write( "Line 423&nbsp;&nbsp;stepSix = " + stepSix );
	document.write( "<BR>" );

	document.write( "Line 426&nbsp;&nbsp;Function roundTwo complete, returning value " + stepSix );
	document.write( "...<BR><BR>" );
	return stepSix;
}	

function roundThree( a, b, c, d, k, s, i ) {
	document.write( "<BR>Line 432&nbsp;&nbsp;Beginning function roundThree( " + a );
	document.write( ", " + b );
	document.write( ", " + c );
	document.write( ", " + d );
	document.write( ", " + k );
	document.write( ", " + s + ", " + i + " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var stepFour;
	var stepFive;
	var stepSix;

	stepOne = MDfuncH( b, c, d );
	document.write( "Line 446&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = addWords( a, stepOne );
	document.write( "Line 449&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = addWords( stepTwo, k );
	document.write( "Line 452&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );
	stepFour = addWords( stepThree, numericToString( i ) );
	document.write( "Line 455&nbsp;&nbsp;stepFour = " + stepFour );
	document.write( "<BR>" );
	stepFive = leftShiftWord( stepFour, s );
	document.write( "Line 458&nbsp;&nbsp;stepFive = " + stepFive );
	document.write( "<BR>" );
	stepSix = addWords( b, stepFive );
	document.write( "Line 461&nbsp;&nbsp;stepSix = " + stepSix );
	document.write( "<BR>" );

	document.write( "Line 464&nbsp;&nbsp;Function roundThree complete, returning value " + stepSix );
	document.write( "...<BR><BR>" );
	return stepSix;
}

function roundFour( a, b, c, d, k, s, i ) {
	document.write( "<BR>Line 470&nbsp;&nbsp;Beginning function roundFour( " + a );
	document.write( ", " + b );
	document.write( ", " + c );
	document.write( ", " + d );
	document.write( ", " + k );
	document.write( ", " + s + ", " + i + " )...<BR>" );
	var stepOne;
	var stepTwo;
	var stepThree;
	var stepFour;
	var stepFive;
	var stepSix;

	stepOne = MDfuncI( b, c, d );
	document.write( "Line 484&nbsp;&nbsp;stepOne = " + stepOne );
	document.write( "<BR>" );
	stepTwo = addWords( a, stepOne );
	document.write( "Line 487&nbsp;&nbsp;stepTwo = " + stepTwo );
	document.write( "<BR>" );
	stepThree = addWords( stepTwo, k );
	document.write( "Line 490&nbsp;&nbsp;stepThree = " + stepThree );
	document.write( "<BR>" );
	stepFour = addWords( stepThree, numericToString( i ) );
	document.write( "Line 493&nbsp;&nbsp;stepFour = " + stepFour );
	document.write( "<BR>" );
	stepFive = leftShiftWord( stepFour, s );
	document.write( "Line 496&nbsp;&nbsp;stepFive = " + stepFive );
	document.write( "<BR>" );
	stepSix = addWords( b, stepFive );
	document.write( "Line 499&nbsp;&nbsp;stepSix = " + stepSix );
	document.write( "<BR>" );

	document.write( "Line 502&nbsp;&nbsp;Function roundFour complete, returning value " + stepSix );
	document.write( "...<BR><BR>" );
	return stepSix;
}

function lengthToPadding( length ) {
	document.write( "<BR>Line 508&nbsp;&nbsp;Beginning function lengthToPadding( " + length + " )...<BR>" );
	lengthPad = new Array;
	var lengthString;

	lengthPad[ 0 ] = ( ( Math.pow( 2, 16 ) - 1 ) & length );
	document.write( "Line 513&nbsp;&nbsp;lengthPad[ 0 ] = " + lengthPad[ 0 ] + "<BR>" );
	lengthPad[ 1 ] = ( ( ( Math.pow( 2, 32 ) - 1 ) - ( Math.pow( 2, 16 ) - 1 ) ) & length );
	document.write( "Line 515&nbsp;&nbsp;lengthPad[ 1 ] = " + lengthPad[ 1 ] + "<BR>" );
	lengthPad[ 2 ] = ( ( ( Math.pow( 2, 48 ) - 1 ) - ( Math.pow( 2, 32 ) - 1 ) ) & length );
	document.write( "Line 517&nbsp;&nbsp;lengthPad[ 2 ] = " + lengthPad[ 2 ] + "<BR>" );
	lengthPad[ 3 ] = ( ( ( Math.pow( 2, 64 ) - 1 ) - ( Math.pow( 2, 48 ) - 1 ) ) & length );
	document.write( "Line 519&nbsp;&nbsp;lengthPad[ 3 ] = " + lengthPad[ 3 ] + "<BR>" );

	lengthString = littleEndianChar( String.fromCharCode( lengthPad[ 0 ] ) );
	document.write( "Line 522&nbsp;&nbsp;lengthString = " + lengthString );
	document.write( "<BR>" );
	lengthString += littleEndianChar( String.fromCharCode( lengthPad[ 1 ] ) );
	document.write( "Line 525&nbsp;&nbsp;lengthString = " + lengthString );
	document.write( "<BR>" );
	lengthString += littleEndianChar( String.fromCharCode( lengthPad[ 2 ] ) );
	document.write( "Line 528&nbsp;&nbsp;lengthString = " + lengthString );
	document.write( "<BR>" );
	lengthString += littleEndianChar( String.fromCharCode( lengthPad[ 3 ] ) );
	document.write( "Line 531&nbsp;&nbsp;lengthString = " + lengthString );
	document.write( "<BR>" );

	document.write( "Line 534&nbsp;&nbsp;Function lengthToPadding complete, returning value " + lengthString );
	document.write( "...<BR><BR>" );
	return lengthString;
}

function wordsInString( string ) {
	document.write( "<BR>Line 540&nbsp;&nbsp;Beginning function wordsInString( " + string );
	document.write( " )...<BR>" );
	var arrayLength;

	document.write( "Line 544&nbsp;&nbsp;string.length = " + string.length + "<BR>" );
	if( string.length % 2 == 1 ) {
		arrayLength = Math.round( ( string.length + 1 ) / 2 );
	} else {
		arrayLength = Math.round( string.length / 2 );
	}

	document.write( "Line 551&nbsp;&nbsp;arrayLength = " + arrayLength + "<BR>" );
	document.write( "Line 552&nbsp;&nbsp;Function wordsInString complete, returning value " + arrayLength + "...<BR><BR>" );
	return arrayLength;
}

function padString( string ) {
	document.write( "<BR>Line 557&nbsp;&nbsp;Beginning function padString( " + string );
	document.write( " )...<BR>" );
	var paddedString;
	var stringLength;
	var lengthString;
	var padLength;
	var setsOf512;

	stringLength = string.length * 16;
	document.write( "Line 566&nbsp;&nbsp;stringLength = " + stringLength + "<BR>" );

	setsOf512 = Math.ceil( stringLength / 512 );
	document.write( "Line 569&nbsp;&nbsp;setsOf512 = " + setsOf512 + "&nbsp;&nbsp;Math.ceil( stringLength / 512 )<BR>" );

	paddedString = string;
	document.write( "Line 572&nbsp;&nbsp;paddedString = " + paddedString );
	document.write( "<BR>" );
	paddedString += String.fromCharCode( 32768 );
	document.write( "Line 575&nbsp;&nbsp;paddedString = " + paddedString );
	document.write( "<BR>" );

	padLength = ( ( setsOf512 * 32 - ( stringLength / 16 ) ) - 5 );
	document.write( "Line 579&nbsp;&nbsp;padLength = " + padLength + "<BR>" );
	for( var i = padLength; i > 0; i-- ) {
		paddedString += String.fromCharCode( 0 );
		document.write( "Line 582&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;paddedString = " + paddedString );
		document.write( "<BR>" );
	}

	lengthString = lengthToPadding( stringLength );
	document.write( "Line 587&nbsp;&nbsp;lengthString = " + lengthString );
	document.write( "<BR>" );
	paddedString += lengthString;
	document.write( "Line 590&nbsp;&nbsp;paddedString = " + paddedString );
	document.write( "<BR>" );

	document.write( "Line 593&nbsp;&nbsp;Function padString complete, returning value " + paddedString );
	document.write( "...<BR><BR>" );
	return paddedString;
}

function littleEndianChar( char ) {
	document.write( "<BR>Line 599&nbsp;&nbsp;Beginning function littleEndianChar( " + char );
	document.write( " )...<BR>" );
	var charCode;
	var firstByte;
	var newCharCode;
	var theChar;

	charCode = char.charCodeAt( 0 );
	document.write( "Line 607&nbsp;&nbsp;charCode = " + charCode + "<BR>" );
	firstByte = ( charCode << 24 ) >>> 16;
	document.write( "Line 609&nbsp;&nbsp;firstByte = " + firstByte + "<BR>" );
	newCharCode = firstByte;
	document.write( "Line 611&nbsp;&nbsp;newCharCode = " + newCharCode + "<BR>" );
	newCharCode += ( ( charCode << 16 ) >>> 24 );
	document.write( "Line 613&nbsp;&nbsp;newCharCode = " + newCharCode + "<BR>" );
	theChar = String.fromCharCode( newCharCode );
	document.write( "Line 615&nbsp;&nbsp;theChar = " + theChar );
	document.write( "<BR>" );

	document.write( "Line 618&nbsp&nbsp;Function littleEndianChar complete, returning value " + theChar );
	document.write( "...<BR><BR>") ;
	return theChar;
}

function stringToWords( string ) {
	document.write( "<BR>Line 624&nbsp;&nbsp;Beginning function stringToWords( " + string );
	document.write( " )...<BR>" );
	var paddedString;
	var arrayLength;

	paddedString = padString( string );
	document.write( "Line 630&nbsp;&nbsp;paddedString = " + paddedString );
	document.write( "<BR>" );

	arrayLength = wordsInString( paddedString );
	document.write( "Line 634&nbsp;&nbsp;arrayLength = " + arrayLength + "<BR>" );

	wordArray = new Array( arrayLength );
	for( var i = 0; i <= ( arrayLength - 1 ); i++ ) {
		wordArray[i] = "";

		for( var j = 0; j <= 1; j++ ) {
			var stringIndex = ( ( i * 2 ) + j );
			document.write( "Line 642&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;stringIndex = " + stringIndex + "<BR>" );
			wordArray[i] += littleEndianChar( paddedString.charAt( stringIndex ) );
			document.write( "Line 644&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;wordArray[i] = " + wordArray[i] );
			document.write( "<BR>" );
		}
	}

	document.write( "Line 649&nbsp;&nbsp;Function stringToWords complete, returning value " + wordArray );
	document.write( "...<BR><BR>" );
	return wordArray;
}

function numericToString( numeric ) {
	document.write( "<BR>Line 655&nbsp;&nbsp;Beginning function numericToString( " + numeric + " )...<BR>" );
	var char1Code;
	var char2Code;
	var theWord;

	Char1Code = ( numeric << 16 ) >>> 16;
	document.write( "Line 661&nbsp;&nbsp;Char1Code = " + Char1Code + "<BR>" );
	Char2Code = numeric >>> 16;
	document.write( "Line 663&nbsp;&nbsp;Char2Code = " + Char2Code + "<BR>" );

	theWord = String.fromCharCode( Char1Code );
	document.write( "Line 666&nbsp;&nbsp;theWord = " + theWord );
	document.write( "<BR>" );
	theWord += String.fromCharCode( Char2Code );
	document.write( "Line 669&nbsp;&nbsp;theWord = " + theWord );
	document.write( "<BR>" );

	document.write( "Line 672&nbsp;&nbsp;Function numericToString complete, returning value " + theWord );
	document.write( "<BR>" );
	return theWord;
}

function getHexOfDigest( digest ) {
	document.write( "<BR>Line 678&nbsp;&nbsp;Beginning function getHexOfDigest( " + digest + " )...<BR>" );
	var i;
	var j;
	var currentHalfWord;
	var currentHalfWordCode;
	var currentHalfByte;
	var hexDigits = "0123456789ABCDEF";
	var hexString = "";

	document.write( "Line 687&nbsp;&nbsp;digest.length - 1 = " + ( digest.length - 1 ) + "<BR>" );
	for( i = 0; i <= ( digest.length - 1 ); i++ ) {
		currentHalfWord = digest.charAt( i );
		document.write( "Line 690&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;currentHalfWord = " + currentHalfWord + "<BR>" );
		currentHalfWordCode = currentHalfWord.charCodeAt( 0 );
		document.write( "Line 692&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;currentHalfWordCode = " + currentHalfWordCode + "<BR>" );

		for( j = 4; j <= 7; j++ ) {
			currentHalfByte = ( currentHalfWordCode << ( j * 4 ) ) >>> 28;
			document.write( "Line 696&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;currentHalfByte = " + currentHalfByte + "<BR>" );
			hexString += hexDigits.charAt( currentHalfByte );
			document.write( "Line 698&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;hexString = " + hexString + "<BR>" );
		}
	}

	document.write( "Line 702&nbsp;&nbsp;Function getHexOfDigest complete, returning value " + hexString + "...<BR><BR>" );
	return hexString;
}

function processMD5( string ) {
	document.write( "Line 707&nbsp;&nbsp;Beginning function processMD5( " + string );
	document.write( " )...<BR>" );
	var wordA;
	var wordB;
	var wordC;
	var wordD;
	var lastWordA;
	var lastWordB;
	var lastWordC;
	var lastWordD;
	var i;
	var j;
	var messageDigest;
	var theDigest;

	wordArray = new Array;
	currentWordBlock = new Array( 16 );
	
	wordArray = stringToWords( string );
	document.write( "Line 726&nbsp;&nbsp;wordArray = " + wordArray );
	document.write( "<BR>" );

	lastWordA = String.fromCharCode(   291 ) + String.fromCharCode( 17767 );
	lastWordB = String.fromCharCode( 35243 ) + String.fromCharCode( 52719 );
	lastWordC = String.fromCharCode( 65244 ) + String.fromCharCode( 47768 );
	lastWordD = String.fromCharCode( 30292 ) + String.fromCharCode( 12816 );
	document.write( "Line 733&nbsp;&nbsp;lastWordA = " + lastWordA );
	document.write( "<BR>" );
	document.write( "Line 735&nbsp;&nbsp;lastWordB = " + lastWordB );
	document.write( "<BR>" );
	document.write( "Line 737&nbsp;&nbsp;lastWordC = " + lastWordC );
	document.write( "<BR>" );
	document.write( "Line 739&nbsp;&nbsp;lastWordD = " + lastWordD );
	document.write( "<BR>" );

	wordA = lastWordA;
	wordB = lastWordB;
	wordC = lastWordC;
	wordD = lastWordD;
	document.write( "Line 746&nbsp;&nbsp;wordA = " + wordA );
	document.write( "<BR>" );
	document.write( "Line 748&nbsp;&nbsp;wordB = " + wordB );
	document.write( "<BR>" );
	document.write( "Line 750&nbsp;&nbsp;wordC = " + wordC );
	document.write( "<BR>" );
	document.write( "Line 752&nbsp;&nbsp;wordD = " + wordD );
	document.write( "<BR>" );
	
	document.write( "Line 755&nbsp;&nbsp;( wordArray.length / 16 ) - 1 = " + ( ( wordArray.length / 16 ) - 1 ) + "<BR>" );
	for( i = 0; i <= ( wordArray.length / 16 ) - 1; i++ ) {
		for( j = 0; j <= 15; j++ ) {
			document.write( "Line 758&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;( 16 * i ) + j = " + ( ( 16 * i ) + j ) );
			document.write( "&nbsp;&nbsp;wordArray[ ( 16 * i ) + j ] = " + wordArray[ ( 16 * i ) + j ] );
			document.write( "<BR>" );
			currentWordBlock[j] = wordArray[ ( 16 * i ) + j ];
			document.write( "Line 762&nbsp;&nbsp;i = " + i + "&nbsp;&nbsp;j = " + j + "&nbsp;&nbsp;currentWordBlock[j] = " + currentWordBlock[j] );
			document.write( "<BR>" );
		}

		lastWordA =   roundOne( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  0 ],  7, 3614090360 );
		document.write( "Line 767&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundOne( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  1 ], 12, 3905402710 );
		document.write( "Line 770&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundOne( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  2 ], 17,  606105819 );
		document.write( "Line 773&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundOne( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  3 ], 22, 3250441966 );
		document.write( "Line 776&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundOne( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  4 ],  7, 4118548399 );
		document.write( "Line 780&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundOne( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  5 ], 12, 1200080426 );
		document.write( "Line 783&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundOne( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  6 ], 17, 2821735955 );
		document.write( "Line 786&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundOne( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  7 ], 22, 4249261313 );
		document.write( "Line 789&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundOne( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  8 ],  7, 1770035416 );
		document.write( "Line 793&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundOne( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  9 ], 12, 2336552879 );
		document.write( "Line 796&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundOne( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 10 ], 17, 4294925233 );
		document.write( "Line 799&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundOne( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 11 ], 22, 2304563134 );
		document.write( "Line 802&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundOne( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[ 12 ],  7, 1804603682 );
		document.write( "Line 806&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundOne( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 13 ], 12, 4254626195 );
		document.write( "Line 809&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundOne( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 14 ], 17, 2792965006 );
		document.write( "Line 812&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundOne( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 15 ], 22, 1236535329 );
		document.write( "Line 815&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );


		lastWordA =   roundTwo( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  1 ],  5, 4129170786 );
		document.write( "Line 820&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundTwo( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  6 ],  9, 3225465664 );
		document.write( "Line 823&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundTwo( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 11 ], 14,  643717713 );
		document.write( "Line 826&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundTwo( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  0 ], 20, 3921069994 );
		document.write( "Line 829&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundTwo( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  5 ],  5, 3593408605 );
		document.write( "Line 833&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundTwo( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 10 ],  9,   38016083 );
		document.write( "Line 836&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundTwo( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 15 ], 14, 3634488961 );
		document.write( "Line 839&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundTwo( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  4 ], 20, 3889429448 );
		document.write( "Line 842&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundTwo( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  9 ],  5,  568446438 );
		document.write( "Line 846&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundTwo( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 14 ],  9, 3275163606 );
		document.write( "Line 849&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundTwo( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  3 ], 14, 4107603335 );
		document.write( "Line 852&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundTwo( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  8 ], 20, 1163531501 );
		document.write( "Line 855&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =   roundTwo( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[ 13 ],  5, 2850285829 );
		document.write( "Line 859&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =   roundTwo( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  2 ],  9, 4243563512 );
		document.write( "Line 862&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =   roundTwo( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  7 ], 14, 1735328473 );
		document.write( "Line 865&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =   roundTwo( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 12 ], 20, 2368359562 );
		document.write( "Line 868&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );


		lastWordA = roundThree( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  5 ],  4, 4294588738 );
		document.write( "Line 873&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD = roundThree( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  8 ], 11, 2272392833 );
		document.write( "Line 876&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC = roundThree( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 11 ], 16, 1839030562 );
		document.write( "Line 879&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB = roundThree( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 14 ], 23, 4259657740 );
		document.write( "Line 882&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA = roundThree( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  1 ],  4, 2763975236 );
		document.write( "Line 886&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD = roundThree( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  4 ], 11, 1272893353 );
		document.write( "Line 889&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC = roundThree( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  7 ], 16, 4139469664 );
		document.write( "Line 892&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB = roundThree( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 10 ], 23, 3200236656 );
		document.write( "Line 895&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA = roundThree( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[ 13 ],  4,  681279174 );
		document.write( "Line 899&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD = roundThree( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  0 ], 11, 3936430074 );
		document.write( "Line 902&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC = roundThree( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  3 ], 16, 3572457605 );
		document.write( "Line 905&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB = roundThree( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  6 ], 23,   76029189 );
		document.write( "Line 908&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA = roundThree( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  9 ],  4, 3654602809 );
		document.write( "Line 912&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD = roundThree( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 12 ], 11, 3873151461 );
		document.write( "Line 915&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC = roundThree( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 15 ], 16,  530742520 );
		document.write( "Line 918&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB = roundThree( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  2 ], 23, 3299628645 );
		document.write( "Line 921&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );


		lastWordA =  roundFour( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  0 ],  6, 4096336452 );
		document.write( "Line 926&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =  roundFour( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  7 ], 10, 1126891415 );
		document.write( "Line 929&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =  roundFour( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 14 ], 15, 2878612391 );
		document.write( "Line 932&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =  roundFour( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  5 ], 21, 4237533241 );
		document.write( "Line 935&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =  roundFour( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[ 12 ],  6, 1700485571 );
		document.write( "Line 939&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =  roundFour( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[  3 ], 10, 2399980690 );
		document.write( "Line 942&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =  roundFour( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[ 10 ], 15, 4293915773 );
		document.write( "Line 945&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =  roundFour( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  1 ], 21, 2240044497 );
		document.write( "Line 948&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =  roundFour( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  8 ],  6, 1873313359 );
		document.write( "Line 952&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =  roundFour( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 15 ], 10, 4264355552 );
		document.write( "Line 955&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =  roundFour( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  6 ], 15, 2734768916 );
		document.write( "Line 958&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =  roundFour( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[ 13 ], 21, 1309151649 );
		document.write( "Line 961&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		lastWordA =  roundFour( lastWordA, lastWordB, lastWordC, lastWordD, currentWordBlock[  4 ],  6, 4149444226 );
		document.write( "Line 965&nbsp;&nbsp;lastWordA = " + lastWordA );
		document.write( "<BR>" );
		lastWordD =  roundFour( lastWordD, lastWordA, lastWordB, lastWordC, currentWordBlock[ 11 ], 10, 3174756917 );
		document.write( "Line 968&nbsp;&nbsp;lastWordD = " + lastWordD );
		document.write( "<BR>" );
		lastWordC =  roundFour( lastWordC, lastWordD, lastWordA, lastWordB, currentWordBlock[  2 ], 15,  718787259 );
		document.write( "Line 971&nbsp;&nbsp;lastWordC = " + lastWordC );
		document.write( "<BR>" );
		lastWordB =  roundFour( lastWordB, lastWordC, lastWordD, lastWordA, currentWordBlock[  9 ], 21, 3951481745 );
		document.write( "Line 974&nbsp;&nbsp;lastWordB = " + lastWordB );
		document.write( "<BR>" );

		wordA = addWords( wordA, lastWordA );
		wordB = addWords( wordB, lastWordB );
		wordC = addWords( wordC, lastWordC );
		wordD = addWords( wordD, lastWordD );
		document.write( "Line 981&nbsp;&nbsp;wordA = " + wordA );
		document.write( "<BR>" );
		document.write( "Line 983&nbsp;&nbsp;wordB = " + wordB );
		document.write( "<BR>" );
		document.write( "Line 985&nbsp;&nbsp;wordC = " + wordC );
		document.write( "<BR>" );
		document.write( "Line 987&nbsp;&nbsp;wordD = " + wordD );
		document.write( "<BR>" );
	}	

	messageDigest = wordA;
	document.write( "Line 992&nbsp;&nbsp;messageDigest = " + messageDigest );
	document.write( "<BR>" );
	messageDigest += wordB;
	document.write( "Line 995&nbsp;&nbsp;messageDigest = " + messageDigest );
	document.write( "<BR>" );
	messageDigest += wordC;
	document.write( "Line 998&nbsp;&nbsp;messageDigest = " + messageDigest );
	document.write( "<BR>" );
	messageDigest += wordD;
	document.write( "Line 1001&nbsp;&nbsp;messageDigest = " + messageDigest );
	document.write( "<BR>" );

	theDigest = getHexOfDigest( messageDigest );
	document.write( "Line 1005&nbsp;&nbsp;theDigest = " + theDigest );
	document.write( "<BR>" );

	document.write( "Line 1008&nbsp;&nbsp;Function processMD5 complete, returning value " + theDigest + "<BR>" );
	return theDigest;
}